# Plot the Launhardt+ rotation curve and our fit to it
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import sys
import numpy.polynomial.polynomial as poly
from astropy.io.ascii import read as asciiread
_path_to_vader = '/Users/krumholz/Projects/viscdisk/vader'
sys.path.append(_path_to_vader)
import vader
sys.path.pop()
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
kpc = 1.0e3*pc
yr = astropyu.yr.to('s')
Myr = 1e6*yr
kmps = 1e5

# Read parameters
paramFile = 'cmzdisk.param'
paramDict = vader.readParam(paramFile)

# Get mass versus r and build vphi vs. r for the Launhard data
mvsr = asciiread(paramDict['m_vs_r_file'])
ndata = len(mvsr['col1'].data)
rotcurvetab = np.zeros((2, ndata))
rotcurvetab[0,:]=mvsr['col1'].data*pc
rotcurvetab[1,:]=np.sqrt(G*mvsr['col2'].data*Msun/rotcurvetab[0,:])
paramDict['rmax'] = 450*pc

# Compute beta for Launhardt data without any interpolation
betatab = np.log(rotcurvetab[1,1:]/rotcurvetab[1,:-1]) / \
          np.log(rotcurvetab[0,1:]/rotcurvetab[0,:-1])

# Construct grid
grd = vader.grid(paramDict, rotCurveTab=rotcurvetab)

# Construct grid for the Bhattacharjee+ 14 data
pD = { 'nr' : 512,
       'grid_type' : 'logarithmic' }
bdata = asciiread('bhattacharjee14.dat')
brotCurve = np.zeros((2,50))
brotCurve[0,:] = bdata['col3']*kpc
brotCurve[1,:] = bdata['col4']*kmps
pD['rmin'] = 0.2*kpc
pD['rmax'] = 6.0*kpc
pD['bspline_degree'] = 3
pD['bspline_breakpoints'] = 6
tmp=np.copy(brotCurve[:,brotCurve[0,:]<7*kpc])
grdb = vader.grid(pD, rotCurveTab=np.ascontiguousarray(tmp))

# Compute kappa and Omega for both data sets
kappal = np.sqrt(2.0*(1.0+grd.beta_g)*grd.vphi_g**2/grd.r_g**2)
kappab = np.sqrt(2.0*(1.0+grdb.beta_g)*grdb.vphi_g**2/grdb.r_g**2)
omegal = grd.vphi_g/grd.r_g
omegab = grdb.vphi_g/grdb.r_g

# Pattern speed for galactic bar
omegabar = 0.06/Myr

# Plot
fig = plt.figure(1, figsize=(6,8))
plt.clf()
ax1 = fig.add_subplot(2,1,1)
ax1.plot(grd.r_g/pc, grd.vphi_g/kmps, 'b', lw=3)
ax1.plot(rotcurvetab[0,:]/pc, rotcurvetab[1,:]/kmps, 'bo')
ax1.plot(grdb.r_g/pc, grdb.vphi_g/kmps, 'g', lw=2)
ax1.plot(brotCurve[0,:]/pc, brotCurve[1,:]/kmps, 'gs')
ax1.set_xlim([10, 6e3])
ax1.set_ylim([0,300])
ax1.set_ylabel('$v_\phi$ [km s$^{-1}$]')
ax1.set_xscale("log")
ax1.set_xticklabels([])

ax2 = ax1.twinx()
ax2.plot(grd.r_h/pc, 1-grd.beta_h, 'b--', lw=3)
ax2.plot(np.sqrt(rotcurvetab[0,1:]*rotcurvetab[0,:-1])/pc,
         1.0-betatab, 'b:', lw=3)
ax2.set_ylabel(r'$1-\beta$')
ax2.set_xlim([10,6e3])
ax2.set_ylim([0,1.4])
ax2.set_xscale("log")
ax2.set_xticklabels([])

line1 = mlines.Line2D([], [], color='b', lw=3, marker='o', 
                      label=r'$v_\phi$ (L+02)')
line2 = mlines.Line2D([], [], color='g', lw=2, marker='s',
                      label=r'$v_\phi$ (B+14)')
line3 = mlines.Line2D([], [], color='b', linestyle='--',
                      lw=3, label=r'$1-\beta$ (L+02, spline)')
line4 = mlines.Line2D([], [], color='b', linestyle=':',
                      lw=3, label=r'$1-\beta$ (L+02, linear)')
ax2.legend(handles=[line1, line2, line3, line4],
           loc='lower right', numpoints=1)
ax1.set_ylim([0,300])

ax = fig.add_subplot(2,1,2)
p1,=ax.plot(grd.r_g/pc, (omegal-kappal/2.0)*Myr, 'b', lw=3)
p2,=ax.plot(grdb.r_g/pc, (omegab-kappab/2.0)*Myr, 'g', lw=1)
p3,=ax.plot([10,5e3], omegabar*Myr*np.ones(2), 'k--')
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlim([10,5e3])
ax.set_ylim([1e-2,8])
ax.set_ylabel(r'Frequency (Myr$^{-1}$)')
ax.legend([p1,p2,p3], 
          [r'$\Omega-\kappa/2$ (L+02)',
           r'$\Omega-\kappa/2$ (B+14)',
           r'$\Omega_{\mathrm{bar}}$'],
          loc='upper right')
ax.set_xlabel('$r$ [pc]')

plt.subplots_adjust(hspace=0, top=0.95, left=0.15)

plt.savefig('rotcurve.pdf')


# Montenegro+'s parameters for various m, surface density, velocity
# dispersion
m_arr = np.array([2])
surfden_arr = np.array([10.,100.])*Msun/pc**2
cs_arr = np.array([20., 80.])*kmps
tgrowthl = np.zeros((len(m_arr), len(surfden_arr), len(cs_arr), len(grd.r_g)))

for h, m in enumerate(m_arr):
    for j, surfden in enumerate(surfden_arr):
        for k, cs in enumerate(cs_arr):
            T1l = -(2.0*m*omegal/(kappal*grd.r_g))**2*(grd.beta_g-1)
            kcritl = kappal**2 / (2.0*np.pi*G*surfden)
            Ql = kappal*cs / (np.pi*G*surfden)
            Jl = np.sqrt(T1l)/kcritl

            # Find growth time for fastest growing modes of acoustic 
            # instability
            coef = np.zeros(6)
            for i in range(len(Jl)):

                # Coefficients in polynomial for wavenumber at most
                # unstable mode
                coef[0] = Ql[i]**4
                coef[1] = -6*Ql[i]**2
                coef[2] = 8
                coef[5] = -16*Jl[i]**2

                # Solve for most unstable wavenumber; discard non-real and
                # negative real roots
                roots = poly.polyroots(coef)
                eta = roots[
                    np.logical_and(np.real(roots)>0, 
                                   np.abs(np.imag(roots))<1.e-15)]

                # Plug candidates for most unstable wavenumber into
                # dispersion relation get dimensionless frequency
                rootpart = np.sqrt((Ql[i]**2-4*eta)*
                                   (Ql[i]**2-4*eta-16*Jl[i]**2*eta**4))
                rootpart = np.array([rootpart, -rootpart])/(8*eta**2)
                otherpart = (Ql[i]**2-4*eta+8*eta**2) / (8*eta**2)
                nu = np.sqrt(otherpart + rootpart)

                # Get maximum imaginary part; this gives fastest
                # growing mode
                nufast = np.amax(np.imag(nu))

                # Convert to growth time measured in orbital periods
                tgrowthl[h,j,k,i] = 1.0 / (nufast*kappal[i]) / \
                                    (2.0*np.pi/omegal[i])


plt.figure(2, figsize=(6,4))
plt.clf()
plt.plot(grd.r_g/pc, tgrowthl[0,0,0,:], 'b', lw=3, 
         label=r'$\Sigma=10$ $M_\odot$ pc$^{-2}$, $\sigma=20$ km s$^{-1}$')
plt.plot(grd.r_g/pc, tgrowthl[0,0,1,:], 'g--', lw=3, 
         label=r'$\Sigma=10$ $M_\odot$ pc$^{-2}$, $\sigma=80$ km s$^{-1}$')
plt.plot(grd.r_g/pc, tgrowthl[0,1,0,:], 'r:', lw=3, 
         label=r'$\Sigma=100$ $M_\odot$ pc$^{-2}$, $\sigma=20$ km s$^{-1}$')
plt.plot(grd.r_g/pc, tgrowthl[0,1,1,:], 'm-.', lw=3, 
         label=r'$\Sigma=100$ $M_\odot$ pc$^{-2}$, $\sigma=80$ km s$^{-1}$')
plt.legend(loc='upper left')
plt.xlabel(r'$r$ [pc]')
plt.ylabel(r'$t_{\mathrm{growth}}/t_{\mathrm{orb}}$')
plt.ylim([0.1,200])
plt.xlim([10,450])
plt.yscale('log')
plt.subplots_adjust(bottom=0.12, left=0.15)
plt.savefig('tgrowth.pdf')
