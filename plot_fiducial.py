# This script plots the results of the fiducial case

# Import libraries
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import numpy.polynomial.polynomial as poly
import ctypes
import sys
import os
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from scipy.interpolate import Akima1DInterpolator as akima
from astropy.io.ascii import read as asciiread
_path_to_vader = '/Users/krumholz/Projects/viscdisk/vader'
sys.path.append(_path_to_vader)
import vader
sys.path.pop()

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
kmps = 1e5

# Times to plot
tplot = np.array([0, 10, 25, 50])*Myr

# Read parameters
paramFile = 'cmzdisk.param'
paramDict = vader.readParam(paramFile)

# Get mass versus r and build vphi vs. r
mvsr = asciiread(paramDict['m_vs_r_file'])
ndata = len(mvsr['col1'].data)
rotcurvetab = np.zeros((2, ndata))
rotcurvetab[0,:]=mvsr['col1'].data*pc
rotcurvetab[1,:]=np.sqrt(G*mvsr['col2'].data*Msun/rotcurvetab[0,:])

# Construct grid
grd = vader.grid(paramDict, rotCurveTab=rotcurvetab)

# Read the simulation results
data = np.load('fiducial.npz')
tOut = data['tOut']
col = data['col']
pres = data['pres']
mBnd = data['mBnd']

# Scale height
zetad = 0.33
fshape = paramDict['shapefac']
rhostar = fshape*(1.0+2.0*grd.beta)*grd.vphi**2/(4.0*np.pi*G*grd.r**2)
acoef = 2.0*np.pi*zetad*G*rhostar*col
bcoef = 0.5*np.pi*G*col**2
ccoef = -pres
hg = (-bcoef + np.sqrt(bcoef**2-4.0*acoef*ccoef)) / (2.0*acoef)

# Compute Q and GI instability growth time
vdisp = np.sqrt(pres/col)
omega = grd.vphi/grd.r
t_orb = 2.0*np.pi/omega
kappa = np.sqrt(2*(grd.beta+1))*omega
kcrit = kappa**2 / (2.0*np.pi*G*col)
Q = kappa*vdisp / (np.pi*G*col)
nu = np.sqrt(1.0 - 1.0/Q**2 + 0j)
tgrowth_GI = 1.0 / (kappa*np.imag(nu)+1e-50) / t_orb
etahat_GI = Q**2/2.0
khat_GI = kcrit*etahat_GI
wl_GI = 2.0*np.pi/khat_GI
wlra_GI = np.zeros(col.shape+(2,))
etahatra_GI = 0.5*(1-np.sqrt(1-Q**2))
wlra_GI[:,:,0] = 2.0*np.pi/(kcrit*etahatra_GI)
etahatra_GI = 0.5*(1+np.sqrt(1-Q**2))
wlra_GI[:,:,1] = 2.0*np.pi/(kcrit*etahatra_GI)

# Growth time for acoustic instabilities
m = 2
T1 = -(2.0*m*omega/(kappa*grd.r))**2*(grd.beta-1)
J = np.sqrt(T1)/kcrit
tgrowth_ac = np.zeros(col.shape)
wl_ac = np.zeros(col.shape)
wlra_ac = np.zeros(col.shape+(2,))
coef = np.zeros(6)
coef1 = np.zeros(5)

#for i in range(0):
for i in range(col.shape[0]):
    for j in range(col.shape[1]):

        # Coefficients in polynomial for wavenumber at most unstable mode
        coef[0] = Q[i,j]**4
        coef[1] = -6*Q[i,j]**2
        coef[2] = 8
        coef[5] = -16*J[i,j]**2

        # Solve for most unstable wavenumber; discard non-real and
        # negative real roots
        roots = poly.polyroots(coef)
        eta = roots[
            np.logical_and(np.real(roots)>0, 
                           np.abs(np.imag(roots))<1.e-15)]

        # Plug candidates for most unstable wavenumber into
        # dispersion relation get dimensionless frequency; only keep
        # imaginary part, since the real part is just a phase
        D = (Q[i,j]**2-4*eta)*(Q[i,j]**2-4*eta-16*J[i,j]**2*eta**4)
        rootpart = np.sqrt(D)
        rootpart = np.array([rootpart, -rootpart])/(8*eta**2)
        otherpart = (Q[i,j]**2-4*eta+8*eta**2) / (8*eta**2)
        nu = np.imag(np.sqrt(otherpart + rootpart))

        # Set imaginary part of frequency to something small for cases
        # where D > 0, because these correspond to GI rather than
        # acoustic instability
        nu[:, D > 0] = 1e-50

        # Get maximum imaginary part; this gives fastest
        # growing mode
        nufast = np.amax(nu)

        # Get wavelength of fastest growing mode
        idx1 = np.unravel_index(np.argmax(nu), nu.shape)[1]
        etafast = np.real(eta[idx1])
        khat = kcrit[i,j]*etafast
        kfast = np.sqrt(khat**2+m**2/grd.r[j]**2)
        wl_ac[i,j] = 2.0*np.pi/kfast

        # Convert to growth time measured in orbital periods
        tgrowth_ac[i,j] = 1.0 / (nufast*kappa[j]) / t_orb[j]

        # Coefficients in polynomial to find all unstable modes
        coef1[0] = Q[i,j]**2/4
        coef1[1] = -1
        coef1[4] = -4*J[i,j]**2

        # Find real zeros of D, and get sign in each interval
        Droots = poly.polyroots(coef1)
        Droots = np.real(Droots[
            np.logical_and(
                np.real(Droots) > 0,
                np.abs(np.imag(Droots))<1.e-15
            )])
        Droots = np.append(Droots, [0, Q[i,j]**2/4,1e50])
        Droots.sort()
        etamid = 0.5*(Droots[1:]+Droots[:-1])
        Dmid = (Q[i,j]**2/(4*etamid**2)-1/etamid) * \
               (Q[i,j]**2/(4*etamid**2)-1/etamid -
                4*J[i,j]**2*etamid**2)
        idx1 = np.where(Dmid < 0)[0]
        for l in idx1:
            etahat = np.array([Droots[l], Droots[l+1]])
            khat = kcrit[i,j]*etahat
            kfast = np.sqrt(khat**2+m**2/grd.r[j]**2)
            wl = 2.0*np.pi/kfast
            wlra_ac[i,j,:] = wl

# Compute alpha and mdot
alpha = np.exp(1.0 - np.minimum(tgrowth_GI, tgrowth_ac))
alpha[alpha < 1.0e-3] = 1.0e-3
alpha[alpha > 1.0] = 1.0
mDotIn = np.zeros((len(tOut),grd.nr+1))
mDotIn[:,1:-1] = grd.g_h[1:-1] * \
                 ((1.0-grd.beta[1:]) * grd.r[1:]**2 * 
                  alpha[:,1:] * pres[:,1:] - 
                  (1.0-grd.beta[:-1]) * grd.r[:-1]**2 * 
                  alpha[:,:-1] * pres[:,:-1])
mDotIn[:,0] = -mBnd[:,0]/(tOut+1.0e-50)
mDotIn[:,-1] = -mBnd[:,1]/(tOut+1.0e-50)
mDotIn[0,-1] = -paramDict['obc_pres_val']
vr = mDotIn[:,1:-1] / (2.0*np.pi*grd.r_h[1:-1]*0.5*(col[:,1:]+col[:,:-1]))


# Mass and mass-weighted mean radius in Q < 1 region and Q < 10
# region; use interpolation to make this behave smoothly
rPad = np.pad(grd.r, (1,1), mode='edge')
rPad[0] = grd.r_h[0]
rPad[-1] = grd.r_h[-1]
colPad = np.pad(col, ((0,0),(1,1)), mode='edge')
QPad = np.pad(Q, ((0,0),(1,1)), mode='edge')
QInterp = akima(np.log(rPad), np.log(np.transpose(QPad)))
colInterp = akima(np.log(rPad), np.log(np.transpose(colPad)))
rHR = np.linspace(grd.r_h[0], grd.r_h[-1], 32768)
QHR = np.exp(np.transpose(
    QInterp(0.5*(np.log(rHR[1:])+np.log(rHR[:-1])))))
colHR = np.exp(np.transpose(
    colInterp(0.5*(np.log(rHR[1:])+np.log(rHR[:-1])))))
mUnstable = np.sum(colHR * (QHR<1.01) * np.pi *
                   (rHR[1:]**2-rHR[:-1]**2), axis=1)
rUnstable = np.sum(np.sqrt(rHR[1:]*rHR[:-1]) * colHR * (QHR<1.01) * np.pi *
                   (rHR[1:]**2-rHR[:-1]**2), axis=1) / (mUnstable + 1.0e-30)
mQ10 = np.sum(colHR * (QHR<10) * np.pi *
             (rHR[1:]**2-rHR[:-1]**2), axis=1)
rQ10 = np.sum(np.sqrt(rHR[1:]*rHR[:-1]) * colHR * (QHR<10) * np.pi *
             (rHR[1:]**2-rHR[:-1]**2), axis=1) / (mQ10 + 1.0e-30)

# Make plots
plt.figure(1, figsize=(6,14))
plt.clf()
rra = [grd.r[0]/pc, grd.r[-1]/pc]
idx = np.array([np.argmin(np.abs(t - tOut)) for t in tplot])
lw = [1, 3, 3, 1, 3]
linestyles=['-', '-', '--', '--', '-.']
lcolors=['k', 'b', 'g', 'r', 'm']

# Surface density
ax=plt.subplot(6,1,1)
for j, i in enumerate(idx):
    plt.plot(grd.r/pc, col[i,:]/(Msun/pc**2), lw=lw[j], 
             linestyle=linestyles[j], color=lcolors[j],
             label=r'$t = {:4.1f}$ Myr'.format(tOut[i]/Myr))
plt.ylabel(r'$\Sigma$ [$M_\odot/\mathrm{pc}^2$]')
plt.yscale('log')
plt.setp(ax.get_xticklabels(), visible=False)
plt.xlim(rra)
plt.ylim([0.5,1e4])

# Velocity dispersion
ax=plt.subplot(6,1,2)
for j, i in enumerate(idx):
    plt.plot(grd.r/pc, vdisp[i,:]/kmps, lw=lw[j], 
             linestyle=linestyles[j], color=lcolors[j],
             label=r'$t = {:2d}$ Myr'.format(int(np.ceil(tOut[i]/Myr))))
plt.ylabel(r'$\sigma$ [km/s]')
plt.setp(ax.get_xticklabels(), visible=False)
plt.xlim(rra)
plt.ylim([0,80])
#plt.legend(loc='lower right', ncol=2)

# Radial velocity
ax=plt.subplot(6,1,3)
for j, i in enumerate(idx):
    plt.plot(grd.r_h[1:-1]/pc, vr[i,:]/kmps, lw=lw[j], 
             linestyle=linestyles[j], color=lcolors[j],
             label=r'$t = {:2d}$ Myr'.format(int(np.ceil(tOut[i]/Myr))))
plt.ylabel(r'$-v_r$ [km/s]')
plt.setp(ax.get_xticklabels(), visible=False)
plt.xlim(rra)
plt.ylim([0,79])
plt.legend(loc='upper right', ncol=2)

# Inflow rate
ax=plt.subplot(6,1,4)
for j, i in enumerate(idx):
    plt.plot(grd.r_h/pc, mDotIn[i,:]/(Msun/yr), lw=lw[j], 
             linestyle=linestyles[j], color=lcolors[j],
             label=r'$t = {:2d}$ Myr'.format(int(np.ceil(tOut[i]/Myr))))
plt.ylabel(r'$\dot{M}_{\mathrm{in}}$ [$M_\odot/\mathrm{yr}$]')
plt.xlim(rra)
plt.ylim([0, 1.99])
plt.setp(ax.get_xticklabels(), visible=False)

# Toomre Q
ax=plt.subplot(6,1,5)
for j, i in enumerate(idx):
    plt.plot(grd.r/pc, Q[i,:], lw=lw[j], 
             linestyle=linestyles[j], color=lcolors[j],
             label=r'$t = {:4.1f}$ Myr'.format(tOut[i]/Myr))
plt.yscale('log')
plt.ylabel(r'Q')
plt.xlim(rra)
plt.ylim([1e-1,9e3])
plt.setp(ax.get_xticklabels(), visible=False)

# Growth timescale
ax=plt.subplot(6,1,6)
for j, i in enumerate(idx):
    # Find where contiguous regions where acoustic > GI
    idx1 = np.where(tgrowth_ac[i,:] < tgrowth_GI[i,:])[0]
    breakpts = np.where(idx1[:-1] != idx1[1:]-1)[0]
    idxbk = [0]
    for b in breakpts:
        idxbk.append(idx1[b]+1)
        idxbk.append(idx1[b+1])
    idxbk.append(tgrowth_GI.shape[1])
    tgr = np.minimum(tgrowth_ac[i,:], tgrowth_GI[i,:])
    # Loop over break points and plot appropriate line style
    for k in range(0, len(idxbk)-1):
        if tgrowth_ac[i,idxbk[k]] < tgrowth_GI[i,idxbk[k]]:
            plt.plot(grd.r[idxbk[k]:idxbk[k+1]]/pc, 
                     tgr[idxbk[k]:idxbk[k+1]],
                     lw=lw[j], color=lcolors[j])
        else:
            plt.plot(grd.r[idxbk[k]:idxbk[k+1]]/pc, 
                     tgr[idxbk[k]:idxbk[k+1]],
                     lw=lw[j], color=lcolors[j],
                     linestyle='--', dashes=(15,10))
solid_line = mlines.Line2D([], [], color='k', lw=2, label='Acoustic')
dashed_line = mlines.Line2D([], [], color='k', linestyle='--', lw=2, 
                            label='Gravitational', dashes=(15,10))
plt.legend(handles=[solid_line, dashed_line], loc='upper right',
           handlelength=3)
plt.yscale('log')
plt.ylim([1e-1,9.0])
plt.xlim(rra)
plt.ylabel(r'$t_{\mathrm{growth}}/t_{\mathrm{orb}}$')
plt.xlabel('r [pc]')

# Adjust spacing
plt.subplots_adjust(hspace=0, wspace=0.2, bottom=0.07, top=0.97, 
                    left=0.18, right=0.93)

# Save
plt.savefig('fiducial1.pdf')


# Second plot: unstable mass and mean radius of unstable mass versus time
plt.figure(2, figsize=(6,6))
plt.clf()
ax=plt.subplot(2,1,1)
plt.plot(tOut/Myr, mUnstable/(1e6*Msun), 'b', lw=2, label=r'$Q < 1$')
plt.plot(tOut/Myr, mQ10/(1e6*Msun), 'g--', lw=2, 
          dashes=(15,10), label=r'$Q < 10$')
plt.legend(loc='upper left', handlelength=3)
plt.setp(ax.get_xticklabels(), visible=False)
plt.ylabel(r'$M$ [$10^6\,M_\odot$]')

ax=plt.subplot(2,1,2)
plt.plot(tOut[mUnstable>0]/Myr, rUnstable[mUnstable>0]/pc, 'b',
         lw=2, label='Q < 1')
plt.plot(tOut[mQ10>0]/Myr, rQ10[mQ10>0]/pc, 'g--', lw=2, 
          dashes=(15,10), label='Q < 10')
plt.ylabel(r'$\langle r\rangle_M$ [pc]')
plt.xlabel(r'$t$ [Myr]')
plt.xlim([0,50])
plt.ylim([0,99])

plt.subplots_adjust(hspace=0)

# Save
plt.savefig('fiducial2.pdf')


# Third plot: unstable wavelength range
fig = plt.figure(3, figsize=(6,6))
plt.clf()

# Make common plot label
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_ylabel('$\lambda$ [pc]', labelpad=25)

# Set times to plot
idx = [100, 175]

for j, i in enumerate(idx):

    # Make window
    ax = fig.add_subplot(2,1,j+1)

    # Label
    if j==0:
        plt.text(60, 3e2, r'$t = {:4.1f}$ Myr'.format(
            tOut[i]/Myr))
    else:
        plt.text(120, 3e2, r'$t = {:4.1f}$ Myr'.format(
            tOut[i]/Myr))


    # Find where contiguous regions where acoustic > GI
    idx1 = np.where(tgrowth_ac[i,:] < tgrowth_GI[i,:])[0]
    breakpts = np.where(idx1[:-1] != idx1[1:]-1)[0]
    idxbk = [0]
    for b in breakpts:
        idxbk.append(idx1[b]+1)
        idxbk.append(idx1[b+1])
    idxbk.append(tgrowth_GI.shape[1])
    tgr = np.minimum(tgrowth_ac[i,:], tgrowth_GI[i,:])
    # Loop over break points and plot appropriate line style
    for k in range(0, len(idxbk)-1):
        if tgrowth_ac[i,idxbk[k]] < tgrowth_GI[i,idxbk[k]]:
            plt.fill_between(grd.r[idxbk[k]:idxbk[k+1]]/pc, 
                             wlra_ac[i,idxbk[k]:idxbk[k+1],0]/pc,
                             wlra_ac[i,idxbk[k]:idxbk[k+1],1]/pc,
                             facecolor='k', alpha=0.3,
                             edgecolor='none')
            plt.scatter(grd.r[idxbk[k]:idxbk[k+1]]/pc,
                        wl_ac[i,idxbk[k]:idxbk[k+1]]/pc,
                        c=np.log10(tgrowth_ac[i,idxbk[k]:idxbk[k+1]]),
                        vmin=-1, vmax=1, lw=0, cmap=cm.copper_r,
                        marker='s', s=30)
            #plt.plot(grd.r[idxbk[k]:idxbk[k+1]]/pc,
            #         wl_ac[i,idxbk[k]:idxbk[k+1]]/pc,
            #         color=lcolors[j], lw=2)
        else:
            plt.fill_between(grd.r[idxbk[k]:idxbk[k+1]]/pc, 
                             wlra_GI[i,idxbk[k]:idxbk[k+1],0]/pc,
                             wlra_GI[i,idxbk[k]:idxbk[k+1],1]/pc,
                             facecolor='none', hatch='XXXX', 
                             edgecolor='k', color='none')
            plt.fill_between(grd.r[idxbk[k]:idxbk[k+1]]/pc, 
                             wlra_GI[i,idxbk[k]:idxbk[k+1],0]/pc,
                             wlra_GI[i,idxbk[k]:idxbk[k+1],1]/pc,
                             color='k', alpha=0.3,
                             edgecolor='none')
            plt.scatter(grd.r[idxbk[k]:idxbk[k+1]]/pc,
                        wl_ac[i,idxbk[k]:idxbk[k+1]]/pc,
                        c=np.log10(tgrowth_GI[i,idxbk[k]:idxbk[k+1]]),
                        vmin=-1, vmax=1, lw=0, cmap=cm.copper_r,
                        marker='s', s=30)
            #plt.plot(grd.r[idxbk[k]:idxbk[k+1]]/pc,
            #         wl_GI[i,idxbk[k]:idxbk[k+1]]/pc,
            #         color=lcolors[j], lw=2,
            #         linestyle='--', dashes=(15,10))

        # Axes and title
        plt.xlim([50,450])
        plt.ylim([1e-4,5e3])
        plt.xscale('log')
        plt.yscale('log')
        ax.set_xticks([50,100,200,400])
        if j==1:
            plt.xlabel('r [pc]')
            ax.set_xticklabels(['50', '100', '200', '400'])
        else:
            ax.set_xticklabels([])

        # Legend
        if j==0:
            patch1 = mpatches.Patch(facecolor='k', alpha=0.3, 
                                    edgecolor='none')
            patch2 = mpatches.Patch(facecolor='none', hatch='XXXX',
                                    edgecolor='k')
            line1 = mlines.Line2D([], [], color='k', lw=np.sqrt(30))
            plt.legend([patch1, (patch1,patch2), line1],
                       ['Acoustic inst.',
                        'Grav. inst.',
                        'Fastest mode'],
                       loc='upper right')

# Adjust
plt.subplots_adjust(hspace=0, wspace=0, left=0.15, top=0.95, right=0.8)

# Add colorbar
axcbar = fig.add_axes((0.8, 0.1, 0.03, 0.95-0.1))
cbar = plt.colorbar(cax=axcbar)
cbar.set_label(r'$\log\, t_{\mathrm{growth}}/t_{\mathrm{orb}}$')

# Save
plt.savefig('wavelength.pdf')

# Write snapshots to text files
interval = 25
fp = open('diederik/rotcurve.txt', 'w')
fp.write('#        r           vphi          kappa           beta\n')
for i in range(grd.r.shape[0]):
    fp.write("{:10.6f}     {:10.6f}     {:10.6f}     {:10.6f}\n".
             format(grd.r[i]/pc, grd.vphi[i]/kmps, kappa[i]*Myr,
                    grd.beta[i]))
fp.close()

fp = open('diederik/surfden.txt', 'w')
fp.write('#        r  ')
for t in tOut[::interval]:
    fp.write("     t={:4.1f}  ".format(t/Myr))
fp.write('\n')
for i in range(col.shape[1]):
    fp.write("{:10.6f}".format(grd.r[i]/pc))
    for j in range(0,col.shape[0],interval):
        fp.write("  {:10.6e}".format(col[j,i]/(Msun/pc**2)))
    fp.write("\n")
fp.close()

fp = open('diederik/pres.txt', 'w')
fp.write('#        r  ')
for t in tOut[::interval]:
    fp.write("     t={:4.1f}  ".format(t/Myr))
fp.write('\n')
for i in range(col.shape[1]):
    fp.write("{:10.6f}".format(grd.r[i]/pc))
    for j in range(0,col.shape[0],interval):
        fp.write("  {:10.6e}".format(pres[j,i]))
    fp.write("\n")
fp.close()

fp = open('diederik/vdisp.txt', 'w')
fp.write('#        r  ')
for t in tOut[::interval]:
    fp.write("   t={:4.1f}  ".format(t/Myr))
fp.write('\n')
for i in range(vdisp.shape[1]):
    fp.write("{:10.6f}".format(grd.r[i]/pc))
    for j in range(0,vdisp.shape[0],interval):
        fp.write("  {:10.6f}".format(vdisp[j,i]/kmps))
    fp.write("\n")
fp.close()

fp = open('diederik/vdisp.txt', 'w')
fp.write('#        r  ')
for t in tOut[::interval]:
    fp.write("   t={:4.1f}  ".format(t/Myr))
fp.write('\n')
for i in range(vdisp.shape[1]):
    fp.write("{:10.6f}".format(grd.r[i]/pc))
    for j in range(0,vdisp.shape[0],interval):
        fp.write("  {:10.6f}".format(vdisp[j,i]/kmps))
    fp.write("\n")
fp.close()

fp = open('diederik/alpha.txt', 'w')
fp.write('#        r  ')
for t in tOut[::interval]:
    fp.write("   t={:4.1f}  ".format(t/Myr))
fp.write('\n')
for i in range(vdisp.shape[1]):
    fp.write("{:10.6f}".format(grd.r[i]/pc))
    for j in range(0,vdisp.shape[0],interval):
        fp.write("  {:10.6f}".format(alpha[j,i]))
    fp.write("\n")
fp.close()

fp = open('diederik/Q.txt', 'w')
fp.write('#        r  ')
for t in tOut[::interval]:
    fp.write("     t={:4.1f}  ".format(t/Myr))
fp.write('\n')
for i in range(vdisp.shape[1]):
    fp.write("{:10.6f}".format(grd.r[i]/pc))
    for j in range(0,vdisp.shape[0],interval):
        fp.write("  {:10.6e}".format(Q[j,i]))
    fp.write("\n")
fp.close()

fp = open('diederik/tgrowth_ac.txt', 'w')
fp.write('#        r  ')
for t in tOut[::interval]:
    fp.write("     t={:4.1f}  ".format(t/Myr))
fp.write('\n')
for i in range(vdisp.shape[1]):
    fp.write("{:10.6f}".format(grd.r[i]/pc))
    for j in range(0,vdisp.shape[0],interval):
        fp.write("  {:10.6e}".format(tgrowth_ac[j,i]))
    fp.write("\n")
fp.close()

fp = open('diederik/tgrowth_GI.txt', 'w')
fp.write('#        r  ')
for t in tOut[::interval]:
    fp.write("     t={:4.1f}  ".format(t/Myr))
fp.write('\n')
for i in range(vdisp.shape[1]):
    fp.write("{:10.6f}".format(grd.r[i]/pc))
    for j in range(0,vdisp.shape[0],interval):
        fp.write("  {:10.6e}".format(tgrowth_GI[j,i]))
    fp.write("\n")
fp.close()

fp = open('diederik/scaleheight.txt', 'w')
fp.write('#        r  ')
for t in tOut[::interval]:
    fp.write("   t={:4.1f}  ".format(t/Myr))
fp.write('\n')
for i in range(hg.shape[1]):
    fp.write("{:10.6f}".format(grd.r[i]/pc))
    for j in range(0,hg.shape[0],interval):
        fp.write("  {:10.6f}".format(hg[j,i]/pc))
    fp.write("\n")
fp.close()

