# README #

This repository contains all the scripts required to run the simulations and generate the figures in Krumholz & Kruijssen (2015), "A Dynamical Model for the Formation of Gas Rings and Episodic Starbursts Near Galactic Centres", submitted to MNRAS. The scripts are of three types: those that run the simulations, those that read the outputs and produce plots, and data files.

The scripts that run the simulations are:

* fiducial.py -- runs the fiducial model described in the paper
* alpha*.py -- runs the models with varying values of alpha
* fshape*.py -- runs the models with varying values of fshape
* mdot*.py -- runs the models with varying values of Mdot_in
* sigmain*.py -- runs the models with varying values of sigma_in
* fiducial_sf.py -- runs the model with star formation turned on

The scripts that produce the plots are all named plot_*.py. Which plots they produce should mostly be obvious from the file names.

Finally, the following data files are included:

* cmzdisk.param -- the VADER parameter file used in the fiducial run
* launhardt02.dat -- tabulated mass data taken from [Launhardt et al. (2002)](http://adsabs.harvard.edu/abs/2002A%26A...384..112L)
* bhattacharjee14.dat -- tabulated rotation curve from [Bhattacharjee et al. (2014)](http://adsabs.harvard.edu/abs/2014ApJ...785...63B)

Running any of the python scripts requires that the [VADER](https://bitbucket.org/krumholz/vader) code be installed. You will probably have to edit the paths at the tops of the various python files to point to wherever you put it. There is no need to compile VADER manually; assuming you have a sane environment, the python scripts will automatically compile it for you.