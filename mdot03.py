# This script runs a parameter study of how varying alpha changes the
# fiducial result

# Import libraries
import numpy as np
import matplotlib.pyplot as plt
import ctypes
import sys
import os
from scipy.interpolate import Akima1DInterpolator as akima
from astropy.io.ascii import read as asciiread
_path_to_vader = '/Users/krumholz/Projects/viscdisk/vader'
sys.path.append(_path_to_vader)
import vader
sys.path.pop()

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
kmps = 1e5

# Run time and output time
tRun = 50.0
tOutStep = 0.1
tOut = np.linspace(0, tRun*Myr, int(tRun/tOutStep)+1)

# Rebuild VADER c library
print("Rebuilding c library...")
os.system('cd '+_path_to_vader+'; make lib PROB=cmzdisk')

# Read parameters
paramFile = 'cmzdisk.param'
paramDict = vader.readParam(paramFile)

# Get mass versus r and build vphi vs. r
mvsr = asciiread(paramDict['m_vs_r_file'])
ndata = len(mvsr['col1'].data)
rotcurvetab = np.zeros((2, ndata))
rotcurvetab[0,:]=mvsr['col1'].data*pc
rotcurvetab[1,:]=np.sqrt(G*mvsr['col2'].data*Msun/rotcurvetab[0,:])

# Construct grid
grd = vader.grid(paramDict, rotCurveTab=rotcurvetab)

# Initialize
col = np.zeros(grd.nr)+paramDict['init_col']*Msun/pc**2
pres = col*(paramDict['init_vdisp']*kmps)**2
gamma = paramDict['gamma']
paramDict['ibc_enth_val'] = gamma/(gamma-1.0) * pres[0]/col[0]

# Set Mdot
paramDict['obc_pres_val'] = -6.3e25*10.**-0.5

# Set up parameters to pass to c
paramArr = np.array([paramDict['eta'], 
                     paramDict['sigmath']*kmps,
                     paramDict['shapefac'], 
                     paramDict['zetad'],
                     paramDict['alphacoef']], dtype='d')
paramPtr \
    = ctypes.byref(paramArr.ctypes.
                   data_as(ctypes.POINTER(ctypes.c_double)).
                   contents)

# Run the simulation
vaderOut = vader.driver(0, tOut[-1], grd, col, pres,
                        paramDict, outTimes = tOut,
                        c_params = paramPtr)

# Save results
np.savez('mdot03.npz', tOut=tOut, col=vaderOut.colOut,
             pres=vaderOut.presOut, mBnd=vaderOut.mBndOut)
